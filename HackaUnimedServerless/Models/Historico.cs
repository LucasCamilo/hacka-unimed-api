﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackaUnimedServerless.Models
{
    public class Historico
    {
        public Aprazamento aprazamento { get; set; }
        public string cid { get; set; }
        public string descricao { get; set; }
        public Medico medico { get; set; }
        public string risco { get; set; }
    }
}
