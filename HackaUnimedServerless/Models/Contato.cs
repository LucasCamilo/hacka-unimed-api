﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackaUnimedServerless.Models
{
    public class Contato
    {
        public string ddd { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
    }
}
