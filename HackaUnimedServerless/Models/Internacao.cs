﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackaUnimedServerless.Models
{
    public class Internacao
    {
        public DateTime DataEntrada { get; set; }
        public DateTime DataSaida { get; set; }
        public Hospital Hospital { get; set; }
    }
}
