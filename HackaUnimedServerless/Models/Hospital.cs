﻿namespace HackaUnimedServerless.Models
{
    public class Hospital
    {
        public string Nome { get; set; }
        public Endereco Endereco { get; set; }
    }
}
