﻿using Amazon.DynamoDBv2.DataModel;
using System.Collections.Generic;
using System;

namespace HackaUnimedServerless.Models
{
    public class Beneficiario
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        public string carteirinha { get; set; }
        public string carteirinhaTitular { get; set; }
        public Contato contato { get; set; }
        public string cpf { get; set; }
        public DateTime dataNascimento { get; set; }
        public Endereco endereco { get; set; }
        public string estadoCivil { get; set; }
        public List<Historico> historico { get; set; }
        public int idade { get; set; }
        public string nomeCompleto { get; set; }
        public PerfilSaude perfilSaude { get; set; }
        public string sexo { get; set; }
        public string tipoUsuario { get; set; }
        public DateTime CreatedTimestamp { get; set; }
    }
}
