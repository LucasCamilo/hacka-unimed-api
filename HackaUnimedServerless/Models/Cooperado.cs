﻿using System.Collections.Generic;

namespace HackaUnimedServerless.Models
{
    public class Cooperado
    {
        public string Nome { get; set; }
        public string CRM { get; set; }
        public List<Especialidade> Especialidades { get; set; }
    }
}
