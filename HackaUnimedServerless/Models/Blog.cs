using System;
using Amazon.DynamoDBv2.DataModel;

namespace HackaUnimedServerless.Models
{
    public class Blog
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Test { get; set; }
        public DateTime CreatedTimestamp { get; set; }
    }
}
