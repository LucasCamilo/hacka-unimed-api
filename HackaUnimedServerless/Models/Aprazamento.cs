﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackaUnimedServerless.Models
{
    public class Aprazamento
    {
        public DateTime dataProximoAtendimento { get; set; }
        public DateTime dataUltimoAtendimento { get; set; }
    }
}
