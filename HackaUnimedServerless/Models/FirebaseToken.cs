﻿using Amazon.DynamoDBv2.DataModel;
using System;

namespace HackaUnimedServerless.Models
{
    public class FirebaseToken
    {
        [DynamoDBHashKey]
        public string Id { get; set; }
        public string token { get; set; }
        public DateTime CreatedTimestamp { get; set; }
    }
}
