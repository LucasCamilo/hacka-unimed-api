﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackaUnimedServerless.Models
{
    public class PerfilSaude
    {
        public int altura { get; set; }
        public bool diabetes { get; set; }
        public bool hipertensao { get; set; }
        public double imc { get; set; }
        public List<string> limitacoes { get; set; }
        public Obesidade obesidade { get; set; }
        public int peso { get; set; }
    }
}
