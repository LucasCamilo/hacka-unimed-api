﻿using System;

namespace HackaUnimedServerless.Models
{
    public class Solicitacao
    {
        public DateTime DataSolicitacao { get; set; }
        public DateTime DataValidade { get; set; }
        public string TipoSolicitacao { get; set; }
        public Cooperado Cooperado { get; set; }
    }
}
