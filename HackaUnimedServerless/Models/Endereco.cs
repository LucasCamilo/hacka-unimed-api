﻿namespace HackaUnimedServerless.Models
{
    public class Endereco
    {
        public string bairro { get; set; }
        public string cep { get; set; }
        public string complemento { get; set; }
        public string estado { get; set; }
        public string logradouro { get; set; }
        public int numero { get; set; }
    }
}
