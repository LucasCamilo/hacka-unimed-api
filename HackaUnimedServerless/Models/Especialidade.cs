﻿using Amazon.DynamoDBv2.DataModel;

namespace HackaUnimedServerless.Models
{
    public class Especialidade
    {
        public string Nome { get; set; }
    }
}
