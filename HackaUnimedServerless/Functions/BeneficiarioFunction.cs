﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;

using Newtonsoft.Json;
using HackaUnimedServerless.Models;

namespace HackaUnimedServerless
{
    public class BeneficiarioFunction
    {
        IDynamoDBContext DDBContext { get; set; }

        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        public BeneficiarioFunction()
        {
            var tableName = System.Environment.GetEnvironmentVariable("BeneficiarioTable");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(Beneficiario)] = new Amazon.Util.TypeMapping(typeof(Beneficiario), "BeneficiarioTable");
            var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };
            this.DDBContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        public async Task<APIGatewayProxyResponse> GetBeneficiariosAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            context.Logger.LogLine("Getting beneficiários");
            var search = this.DDBContext.ScanAsync<Beneficiario>(null);
            var page = await search.GetNextSetAsync();
            context.Logger.LogLine($"Found {page.Count} benefiarios");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(page),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };

            return response;
        }

        public async Task<APIGatewayProxyResponse> GetBeneficiarioAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            string beneficiarioId = null;
            if (request.PathParameters != null && request.PathParameters.ContainsKey("Id"))
            {
                beneficiarioId = request.PathParameters["Id"];
            } 
            else if (request.QueryStringParameters != null && request.QueryStringParameters.ContainsKey("Id"))
            {
                beneficiarioId = request.QueryStringParameters["Id"];
            }
                

            if (string.IsNullOrEmpty(beneficiarioId))
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Body = "Missing required parameter Id"
                };
            }

            var beneficiario = await DDBContext.LoadAsync<Beneficiario>(beneficiarioId);

            if (beneficiario == null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.NotFound
                };
            }

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = JsonConvert.SerializeObject(beneficiario),
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            return response;
        }

        public async Task<APIGatewayProxyResponse> AddBeneficiarioAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            var beneficiario = JsonConvert.DeserializeObject<Beneficiario>(request?.Body);
            beneficiario.Id = Guid.NewGuid().ToString();
            beneficiario.CreatedTimestamp = DateTime.Now;

            context.Logger.LogLine($"Saving beneficiario with id {beneficiario.Id}");
            await DDBContext.SaveAsync(beneficiario);
            context.Logger.LogLine($"Saved beneficiario with id {beneficiario.Id}");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = beneficiario.Id.ToString(),
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };
            return response;
        }
    }
}
