﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using HackaUnimedServerless.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace HackaUnimedServerless
{
    public class FirebaseTokenFunction
    {
        IDynamoDBContext DDBContext { get; set; }

        public FirebaseTokenFunction()
        {
            var tableName = System.Environment.GetEnvironmentVariable("FirebaseTokenTable");
            AWSConfigsDynamoDB.Context.TypeMappings[typeof(FirebaseToken)] = new Amazon.Util.TypeMapping(typeof(FirebaseToken), "FirebaseTokenTable");
            var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };
            this.DDBContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        public async Task<APIGatewayProxyResponse> AddFirebaseTokenAsync(APIGatewayProxyRequest request, ILambdaContext context)
        {
            var firebaseToken = JsonConvert.DeserializeObject<FirebaseToken>(request?.Body);

            var search = DDBContext.ScanAsync<FirebaseToken>(null);
            var page = await search.GetNextSetAsync();

            var register = page.FirstOrDefault(x => x.token == firebaseToken.token);

            if(register != null)
            {
                return new APIGatewayProxyResponse
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Body = register.Id.ToString(),
                    Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
                };
            }

            firebaseToken.Id = Guid.NewGuid().ToString();
            firebaseToken.CreatedTimestamp = DateTime.Now;

            context.Logger.LogLine($"Saving token firebase with id {firebaseToken.Id}");
            await DDBContext.SaveAsync(firebaseToken);
            context.Logger.LogLine($"Saved token firebase with id {firebaseToken.Id}");

            var response = new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = firebaseToken.Id.ToString(),
                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
            };
            return response;
        }
    }
}
